//
//  TabBarController.swift
//  Movs
//
//  Created by Murilo Gasparetto on 06/03/2018.
//  Copyright © 2018 Murilo Gasparetto. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        if item.title == "Movies" {
            let nVC1 = selectedViewController as! UINavigationController
            guard let fVC = nVC1.viewControllers[0] as? FavoritesViewController else {
                return
            }
            guard let deleted = fVC.deletedMovie else {
                return
            }
            let nVC = viewControllers![0] as! UINavigationController
            let colVC = nVC.viewControllers[0] as! CollectionViewController
            
            if colVC.isSearching() {
                guard let index = colVC.searchedMovies.index(where: ({$0.id == deleted.id})) else {
                    return
                }
                
                colVC.searchedMovies[index] = deleted
                let indexPath = IndexPath(row: index, section: 0)
                colVC.collectionView?.reloadItems(at: [indexPath])
            } else {
                
                guard let index = DataController.fetchedMovies.index(where: ({$0.id == deleted.id})) else {
                    return
                }
                
                DataController.fetchedMovies[index] = deleted
                let indexPath = IndexPath(row: index, section: 0)
                colVC.collectionView?.reloadItems(at: [indexPath])
            }
        }
    }
    
}
