//
//  FavoritesViewController.swift
//  Movs
//
//  Created by Murilo Gasparetto on 06/03/2018.
//  Copyright © 2018 Murilo Gasparetto. All rights reserved.
//

import UIKit

class FavoritesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var favsTableView: UITableView!
    @IBOutlet weak var resultsTableView: UITableView!
    @IBOutlet weak var removeFilterBtn: UIButton!
    
    var deletedMovie: Movie?
    var isFiltered = false
    var wasEmpty: Bool!
    var filteredResults: [Movie]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if DataController.savedMovies.count == 0 {
            wasEmpty = true
        } else {
            wasEmpty = false
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if DataController.savedMovies.count == 0 {
            wasEmpty = true
        } else {
            wasEmpty = false
        }
        
        if let _ = filteredResults {
            if filteredResults?.count != 0 {
                isFiltered = true
                favsTableView.isHidden = true
                removeFilterBtn.isHidden = false
                resultsTableView.isHidden = false
                resultsTableView.reloadData()
            } else {
                isFiltered = false
                favsTableView.isHidden = false
                removeFilterBtn.isHidden = true
                resultsTableView.isHidden = true
            }
        }
        
        favsTableView.reloadData()
        
        if (DataController.savedMovies.count == 0) && (wasEmpty) {
            favsTableView.separatorStyle = UITableViewCellSeparatorStyle.none
            favsTableView.isScrollEnabled = false
        } else {
            favsTableView.separatorStyle = UITableViewCellSeparatorStyle.singleLine
            favsTableView.isScrollEnabled = true
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (DataController.savedMovies.count == 0) && (wasEmpty){
            return 1
        } else {
            if isFiltered {
                if filteredResults?.count == 0 {
                    return 0
                } else {
                    return (filteredResults?.count)!
                }
            } else {
                
            }
            return DataController.savedMovies.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (DataController.savedMovies.count == 0) && (wasEmpty) {
            return nothingToShowCellAtIndexPath(indexPath: indexPath)
        } else {
            if isFiltered {
                if filteredResults?.count == 0 {
                    return nothingToShowCellAtIndexPath(indexPath: indexPath)
                } else {
                    let cell = resultsTableView.dequeueReusableCell(withIdentifier: "favCell", for: indexPath) as! TableViewCell
                    
                    let movie = filteredResults![indexPath.row]
                    
                    cell.movieImg.image = ImageManager.managerDefault.cacheImgFromPath(path: movie.poster_path)
                    cell.movieTitle.text = movie.title
                    cell.movieYear.text = movie.release_date
                    cell.movieTxt.text = movie.overview
                    
                    return cell
                }
            } else {
                let cell = favsTableView.dequeueReusableCell(withIdentifier: "favCell", for: indexPath) as! TableViewCell
                
                let movie = DataController.savedMovies[indexPath.row]
                
                cell.movieImg.image = ImageManager.managerDefault.cacheImgFromPath(path: movie.poster_path)
                cell.movieTitle.text = movie.title
                cell.movieYear.text = movie.release_date
                cell.movieTxt.text = movie.overview
                
                return cell
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (DataController.savedMovies.count == 0) && (wasEmpty) {
            return tableView.frame.size.height - 100
        } else {
            if isFiltered {
                if filteredResults?.count == 0 {
                    return tableView.frame.size.height - 100
                } else {
                    return 120.0
                }
            } else {
                return 120.0
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (DataController.savedMovies.count == 0) && (wasEmpty) {
        }else {
            self.performSegue(withIdentifier: "showDetail", sender: indexPath)
        }
    }
    
    // Override to support editing the table view.
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if (DataController.savedMovies.count == 0) && (wasEmpty) {
        } else {
            if isFiltered {
                if filteredResults?.count == 0 {
                    
                } else {
                    if editingStyle == .delete {
                        deletedMovie = filteredResults?[indexPath.row]
                        deletedMovie?.fav = false
                        DataController.updateSavedMovies(movie: deletedMovie!)
                        filteredResults?.remove(at: indexPath.row)
                        tableView.deleteRows(at: [indexPath], with: .fade)
                    }
                }
            } else {
                if editingStyle == .delete {
                    deletedMovie = DataController.savedMovies[indexPath.row]
                    deletedMovie?.fav = false
                    DataController.updateSavedMovies(movie: deletedMovie!)
                    tableView.deleteRows(at: [indexPath], with: .fade)
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if (DataController.savedMovies.count == 0) && (wasEmpty) {
            return false } else {
            return true
        }
    }
    
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        if (DataController.savedMovies.count == 0) && (wasEmpty) {
            return false } else {
            return true
        }
    }
    
    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return "Unfavorite"
    }
    
    @IBAction func removeFilterAction(_ sender: Any) {
        self.filteredResults = nil
        isFiltered = false
        favsTableView.isHidden = false
        removeFilterBtn.isHidden = true
        resultsTableView.isHidden = true
        favsTableView.reloadData()
    }
    
    @IBAction func filterAction(_ sender: Any) {
        if DataController.savedMovies.count == 0 {
            let alert = UIAlertController(title: "No Favorite Movie", message: "Try to favorite some", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
            
            self.present(alert, animated: true)
        } else {
            self.performSegue(withIdentifier: "showFilter", sender: self)
        }
    }
    
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            if let dvc = segue.destination as? DetailTableViewController {
                let indexPath = sender as? IndexPath
                if isFiltered {
                    if filteredResults?.count == 0 {
                    } else {
                        dvc.movie = filteredResults?[(indexPath?.row)!]
                    }
                } else {
                    dvc.movie = DataController.savedMovies[(indexPath?.row)!]
                }
                
            }
        }
    }
    
}

extension FavoritesViewController {
    func nothingToShowCellAtIndexPath(indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.textAlignment = NSTextAlignment.center
        cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 24)
        cell.textLabel?.text = "Nothing to show"
        
        return cell
    }
}

