//
//  GenresFilterViewController.swift
//  Movs
//
//  Created by Murilo Gasparetto on 05/03/2018.
//  Copyright © 2018 Murilo Gasparetto. All rights reserved.
//

import UIKit

class GenresFilterViewController: UITableViewController {
    
    var availableGenres = [String]()
    var filteredGenres = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        for m in DataController.savedMovies {
            for g in m.genres {
                if let _ = self.availableGenres.index(of: g){} else {
                    self.availableGenres.append(g)
                }
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if isMovingFromParentViewController {
            let navVC = self.tabBarController?.childViewControllers[1] as! UINavigationController
            let filterVC = navVC.viewControllers[1] as! FilterTableViewController
            filterVC.filteredGenres = self.filteredGenres
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return availableGenres.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "genreCell", for: indexPath)
        
        cell.textLabel?.text = availableGenres[indexPath.row]
        guard let _ = self.filteredGenres.index(of: availableGenres[indexPath.row]) else {
            return cell
        }
        
        cell.accessoryView = UIImageView(image: UIImage(named: "check_icon"))
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let cell = tableView.cellForRow(at: indexPath)
        let genre = cell?.textLabel?.text
        guard let _ = cell?.accessoryView else {
            cell?.accessoryView = UIImageView(image: UIImage(named: "check_icon"))
            self.filteredGenres.append(genre!)
            return
        }
        self.filteredGenres.remove(at: self.filteredGenres.index(of: genre!)!)
        cell?.accessoryView = nil
    }
    
}
