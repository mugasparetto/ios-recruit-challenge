//
//  DetailTableViewController.swift
//  Movs
//
//  Created by Murilo Gasparetto on 28/02/2018.
//  Copyright © 2018 Murilo Gasparetto. All rights reserved.
//

import UIKit

class DetailTableViewController: UITableViewController {
    
    var movie: Movie?
    var btnFav = UIButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 300
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.didEnterBackground(notification:)), name: NSNotification.Name.UIApplicationDidEnterBackground, object: UIApplication.shared)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if (movie?.fav)! {
            btnFav.setImage(UIImage(named: "favorite_full_icon"), for: .normal)
        } else {
            btnFav.setImage(UIImage(named: "favorite_gray_icon"), for: .normal)
        }
    }
    
    @objc func didEnterBackground (notification: NSNotification) {
        DataController.updateSavedMovies(movie: movie!)
        DataController.saveMovies(movies: DataController.savedMovies)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        
        switch indexPath.row {
        case 0:
            let headerView = Bundle.main.loadNibNamed("DetailHeaderView", owner: self, options: nil)?.first as! DetailHeaderView

            guard let image = ImageManager().cacheImgFromPath(path: (movie?.poster_path)!) else {
                return headerView
            }
            
            headerView.activityIndicator.stopAnimating()
            headerView.activityIndicator.isHidden = true
            headerView.imgMovie.isHidden = false
            headerView.imgMovie.image = image
            
            return headerView
        case 1:
            cell.textLabel?.text = movie?.title
            btnFav = (UIButton(type: UIButtonType.custom))
            btnFav.frame = CGRect(x: view.frame.width - 44, y: 0, width: 44, height: 44)
            if !(movie?.fav)! {
                btnFav.setImage(UIImage(named: "favorite_gray_icon"), for: .normal)
            } else {
                btnFav.setImage(UIImage(named: "favorite_full_icon"), for: .normal)
            }
            btnFav.addTarget(self, action: #selector(self.favAction(_:)), for: UIControlEvents.touchUpInside)
            cell.addSubview(btnFav)
            break
        case 2:
            cell.textLabel?.text = movie?.release_date
            break
        case 3:
            cell.textLabel?.numberOfLines = 0
            cell.textLabel?.text = movie?.printableGenres()
        default:
            let footerView = Bundle.main.loadNibNamed("DetailFooterView", owner: self, options: nil)?.first as! DetailFooterView
            
            footerView.movieOverview.text = movie?.overview
            
            return footerView
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    @objc func favAction (_ sender: UIButton) {
        movie?.fav = !(movie?.fav)!
        if (movie?.fav)! {
            btnFav.setImage(UIImage(named: "favorite_full_icon"), for: .normal)
        } else {
            btnFav.setImage(UIImage(named: "favorite_gray_icon"), for: .normal)
        }
        DataController.updateSavedMovies(movie: self.movie!)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        let navVC = self.tabBarController?.childViewControllers[0] as! UINavigationController
        let colVC = navVC.viewControllers[0] as! CollectionViewController
        
        if colVC.isSearching() {
            guard let index = colVC.searchedMovies.index(where: ({$0.id == movie!.id})) else {
                return
            }
            
            colVC.searchedMovies[index] = self.movie!
            let indexPath = IndexPath(row: index, section: 0)
            colVC.collectionView?.reloadItems(at: [indexPath])
        } else {
            
            guard let index = DataController.fetchedMovies.index(where: ({$0.id == movie!.id})) else {
                return
            }
            
            DataController.fetchedMovies[index] = self.movie!
            let indexPath = IndexPath(row: index, section: 0)
            colVC.collectionView?.reloadItems(at: [indexPath])
        }
    }
    
}
