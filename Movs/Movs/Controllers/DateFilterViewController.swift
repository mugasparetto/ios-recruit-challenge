//
//  DateFilterViewController.swift
//  Movs
//
//  Created by Murilo Gasparetto on 05/03/2018.
//  Copyright © 2018 Murilo Gasparetto. All rights reserved.
//

import UIKit

class DateFilterViewController: UITableViewController {
    
    var availableDates = [Int]()
    var filteredDates = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        for m in DataController.savedMovies {
            if let _ = self.availableDates.index(of: Int(m.release_date)!){} else {
                self.availableDates.append(Int(m.release_date)!)
            }
        }
        
        availableDates.sort()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if isMovingFromParentViewController {
            let navVC = self.tabBarController?.childViewControllers[1] as! UINavigationController
            let filterVC = navVC.viewControllers[1] as! FilterTableViewController
            filterVC.filteredDates = self.filteredDates
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return availableDates.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "dateCell", for: indexPath)
        
        cell.textLabel?.text = String(availableDates[indexPath.row])
        guard let _ = self.filteredDates.index(of: String(availableDates[indexPath.row])) else {
            return cell
        }
        
        cell.accessoryView = UIImageView(image: UIImage(named: "check_icon"))
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let cell = tableView.cellForRow(at: indexPath)
        let genre = cell?.textLabel?.text
        guard let _ = cell?.accessoryView else {
            cell?.accessoryView = UIImageView(image: UIImage(named: "check_icon"))
            self.filteredDates.append(genre!)
            return
        }
        self.filteredDates.remove(at: self.filteredDates.index(of: genre!)!)
        cell?.accessoryView = nil
    }
    
}
