//
//  FilterTableViewController.swift
//  Movs
//
//  Created by Murilo Gasparetto on 05/03/2018.
//  Copyright © 2018 Murilo Gasparetto. All rights reserved.
//

import UIKit

class FilterTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    var filteredGenres: [String]?
    var filteredDates: [String]?
    
    override func viewDidLoad() {
        super.viewDidLoad()    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let _ = self.filteredGenres {
            
            let indexPath = IndexPath(row: 1, section: 0)
            tableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.none) }
        
        if let _ = self.filteredDates {
            
            let indexPath2 = IndexPath(row: 0, section: 0)
            tableView.reloadRows(at: [indexPath2], with: UITableViewRowAnimation.none) }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "cell")
        cell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
        cell.detailTextLabel?.font = UIFont.boldSystemFont(ofSize: 17)
        cell.detailTextLabel?.textColor = UIColor(red: 247/255, green: 206/255, blue: 91/255, alpha: 1)
        
        if indexPath.row == 0 {
            cell.textLabel?.text = "Date"
            guard let array = self.filteredDates else {
                return cell
            }
            if array.count == 1 {
                cell.detailTextLabel?.text = filteredDates?[0]
            } else {
                if array.count > 1 {
                    cell.detailTextLabel?.text = "..."
                }
            }
        } else {
            cell.textLabel?.text = "Genres"
            guard let array = self.filteredGenres else {
                return cell
            }
            if array.count == 1 {
                cell.detailTextLabel?.text = filteredGenres?[0]
            } else {
                if array.count > 1 {
                    cell.detailTextLabel?.text = "..."
                }
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.row == 0 {
            performSegue(withIdentifier: "dateSegue", sender: self)
        } else {
            performSegue(withIdentifier: "genresSegue", sender: self)
        }
    }
    
    @IBAction func applyFilters(_ sender: Any) {
        if filteredDates == nil && filteredGenres == nil {
            let alert = UIAlertController(title: "No Filter Conditions", message: "Try to apply some", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
            
            self.present(alert, animated: true)
        } else {
            self.navigationController?.popViewController(animated: true)
            let tvc = self.navigationController?.viewControllers[0] as! FavoritesViewController
            
            if filteredDates != nil && filteredGenres == nil{
                let filteredD = DataController.savedMovies.filter({(filteredDates?.contains($0.release_date))!})
                tvc.filteredResults = filteredD
                return 
            }
            
            if filteredGenres != nil {
                let filteredG = DataController.savedMovies.filter({ (movie) -> Bool in
                    let intersec = Set(movie.genres).intersection(Set(filteredGenres!))
                    if intersec.count > 0 && intersec.count == filteredGenres?.count {
                        return true
                    } else {
                        return false
                    }
                })
                tvc.filteredResults = filteredG
                
                if filteredDates != nil {
                    let filteredDG = filteredG.filter({(filteredDates?.contains($0.release_date))!})
                    tvc.filteredResults = filteredDG
                }
            }
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "genresSegue" {
            let gVC = segue.destination as! GenresFilterViewController
            guard let array = self.filteredGenres else {
                return
            }
            gVC.filteredGenres = array
        } else {
            if segue.identifier == "dateSegue" {
                let dVC = segue.destination as! DateFilterViewController
                guard let array = self.filteredDates else {
                    return
                }
                dVC.filteredDates = array
            }
        }
    }
    
}
