//
//  CollectionViewController.swift
//  Movs
//
//  Created by Murilo Gasparetto on 28/02/2018.
//  Copyright © 2018 Murilo Gasparetto. All rights reserved.
//

import UIKit

private let reuseIdentifier = "movieCell"
private let itemsPerRow: CGFloat = 2
private let sectionInsets = UIEdgeInsets(top: 35.0, left: 20.0, bottom: 35.0, right: 20.0)


class CollectionViewController: UICollectionViewController, CollectionViewCellDelegate {
    
    var shouldFetch = false
    let searchController = UISearchController(searchResultsController: nil)
    var searchedMovies = [Movie]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView?.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "LoadingCell")
        
        setupSearch()
        
        RequestManager.fetchData(displayError: displayErrorView, finishedFeeding: { (newMovies) in
            DataController.fetchedMovies += newMovies
        }, completion: fetchCompleted)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if #available(iOS 11.0, *) {
            navigationItem.hidesSearchBarWhenScrolling = false
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if #available(iOS 11.0, *) {
            navigationItem.hidesSearchBarWhenScrolling = true
        }
    }
    
    func searchBarIsEmpty() -> Bool {
        return searchController.searchBar.text?.isEmpty ?? true
    }
    
    func filterContentForSearchText(_ searchText: String, scope: String = "All") {
        searchedMovies = DataController.fetchedMovies.filter({ (movie) -> Bool in
            return movie.title.lowercased().contains(searchText.lowercased())
        })
        collectionView?.reloadData()
        
        if searchBarIsEmpty() {
            if let error = view.viewWithTag(404) {
                error.removeFromSuperview()
            }
        }
        
        if searchedMovies.count == 0 {
            if isSearching() {
                if let _ = view.viewWithTag(404) {
                } else {
                    displayNoSearchView()
                }
            }
        } else {
            if let error = view.viewWithTag(404) {
                error.removeFromSuperview()
            }
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            if let dvc = segue.destination as? DetailTableViewController {
                let indexPath = sender as? IndexPath
                if isSearching() {
                    dvc.movie = searchedMovies[(indexPath?.row)!]
                } else {
                    dvc.movie = DataController.fetchedMovies[(indexPath?.row)!]
                }
            }
        }
    }
}

// MARK: UICollectionViewDataSource

extension CollectionViewController {
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if isSearching() {
            return searchedMovies.count
        } else {
            return DataController.fetchedMovies.count + 1
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! CollectionViewCell
        
        cell.activityIndicator.isHidden = false
        cell.activityIndicator.startAnimating()
        cell.btnFav.tag = indexPath.row
        
        if isSearching() {
            cell.movieTitle.text = searchedMovies[indexPath.row].title
            
            if searchedMovies[indexPath.row].fav {
                cell.btnFav.setImage(UIImage(named: "favorite_full_icon"), for: .normal)
            } else {
                cell.btnFav.setImage(UIImage(named: "favorite_gray_icon"), for: .normal)
            }
            
            cell.delegate = self
            
            guard let image = ImageManager.managerDefault.cacheImgFromPath(path: searchedMovies[indexPath.row].poster_path) else {
                return cell
            }
            
            cell.activityIndicator.stopAnimating()
            cell.activityIndicator.isHidden = true
            cell.movieImg.image = image
            
            return cell
        } else {
            if indexPath.item < DataController.fetchedMovies.count {
                
                cell.movieTitle.text = DataController.fetchedMovies[indexPath.row].title
                
                if DataController.fetchedMovies[indexPath.row].fav {
                    cell.btnFav.setImage(UIImage(named: "favorite_full_icon"), for: .normal)
                } else {
                    cell.btnFav.setImage(UIImage(named: "favorite_gray_icon"), for: .normal)
                }
                
                cell.delegate = self
                
                guard let image = ImageManager.managerDefault.cacheImgFromPath(path: DataController.fetchedMovies[indexPath.row].poster_path) else {
                    return cell
                }
                
                cell.activityIndicator.stopAnimating()
                cell.activityIndicator.isHidden = true
                cell.movieImg.image = image
                
                return cell
            } else {
                return loadingCellForIndexPath(indexPath: indexPath)
            }
        }
    }
    
    func loadingCellForIndexPath(indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView?.dequeueReusableCell(withReuseIdentifier: "LoadingCell", for: indexPath)
        
        let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)
        activityIndicator.color = UIColor(red: 45/255, green: 48/255, blue: 71/255, alpha: 1)
        let x = (view.frame.width - sectionInsets.left - sectionInsets.right - 37)/2
        activityIndicator.frame.origin = CGPoint(x: x, y: 0)
        
        cell?.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        
        return cell!
    }
    
    // MARK: UICollectionViewDelegate
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "showDetail", sender: indexPath)
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let scrollAmount = scrollView.contentOffset.y
        let bottom = scrollView.contentSize.height - scrollView.bounds.size.height
        if !isSearching() {
            if scrollAmount > bottom && shouldFetch {
                RequestManager.fetchData(displayError: displayErrorView, finishedFeeding: { (newMovies) in
                    DataController.fetchedMovies += newMovies
                }, completion: fetchCompleted)
                shouldFetch = false
            }
        }
    }
    
    func favMovie(tag: NSNumber) {
        let index = tag.intValue
        var movie : Movie
        if isSearching() {
            movie = searchedMovies[index]
        } else {
            movie = DataController.fetchedMovies[index]
        }
        let indexPath = IndexPath(row: index, section: 0)
        let cell = collectionView?.cellForItem(at: indexPath) as! CollectionViewCell
        
        movie.fav = !movie.fav
        if movie.fav {
            cell.btnFav.setImage(UIImage(named: "favorite_full_icon"), for: .normal)
        } else {
            cell.btnFav.setImage(UIImage(named: "favorite_gray_icon"), for: .normal)
        }
        
        DataController.updateSavedMovies(movie: movie)
    }
}

// MARK: UICollectionViewDelegateFlowLayout

extension CollectionViewController : UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {

        if indexPath.row < DataController.fetchedMovies.count {
            let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
            let availableWidth = view.frame.width - paddingSpace
            let widthPerItem = availableWidth / itemsPerRow
            let heightPerItem = widthPerItem*3/2 + 50
            
            return CGSize(width: widthPerItem, height: heightPerItem)
        } else {
            return CGSize(width: view.frame.width - sectionInsets.left - sectionInsets.right, height: 44)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.left
    }
}

extension CollectionViewController {
    func displayErrorView (){
        let errorView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height))
        errorView.backgroundColor = UIColor.white
        errorView.tag = 404
        view.addSubview(errorView)
        
        let errorImg = UIImageView(image: UIImage(named: "alert_icon"))
        errorImg.center = errorView.center
        errorImg.center.y -= 50
        
        let errorTxt = UITextView(frame: CGRect(x: 20, y: errorImg.center.y + 100, width: view.frame.width - 40, height: 200))
        errorTxt.isEditable = false
        errorTxt.isSelectable = false
        errorTxt.font = UIFont.boldSystemFont(ofSize: 31)
        errorTxt.text = "Something wrong happened"
        errorTxt.textAlignment = NSTextAlignment.center
        
        errorView.addSubview(errorImg)
        errorView.addSubview(errorTxt)
    }
    
    func displayNoSearchView (){
        let noSearchView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height))
        noSearchView.backgroundColor = UIColor.white
        noSearchView.tag = 404
        view.addSubview(noSearchView)
        
        let noSearchImg = UIImageView(image: UIImage(named: "search_icon"))
        noSearchImg.center = noSearchView.center
        noSearchImg.center.y = 200
        
        let noSearchTxt = UITextView(frame: CGRect(x: 20, y: noSearchImg.center.y + 100, width: view.frame.width - 40, height: 200))
        noSearchTxt.isEditable = false
        noSearchTxt.isSelectable = false
        noSearchTxt.font = UIFont.boldSystemFont(ofSize: 31)
        noSearchTxt.text = "No results found"
        noSearchTxt.textColor = UIColor(red: 45/255, green: 48/255, blue: 71/255, alpha: 1)
        noSearchTxt.textAlignment = NSTextAlignment.center
        
        noSearchView.addSubview(noSearchImg)
        noSearchView.addSubview(noSearchTxt)
    }
    
    func fetchCompleted() {
        self.collectionView?.reloadData()
        shouldFetch = true
    }
}

extension CollectionViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        filterContentForSearchText(searchController.searchBar.text!)
    }
    
    func setupSearch () {
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search"
        self.navigationItem.searchController = searchController
        definesPresentationContext = true
        searchController.searchBar.tintColor = UIColor(red: 45/255, green: 48/255, blue: 71/255, alpha: 1)
        let cancelButtonAttributes: NSDictionary = [NSAttributedStringKey.foregroundColor: UIColor(red: 45/255, green: 48/255, blue: 71/255, alpha: 1)]
        UIBarButtonItem.appearance().setTitleTextAttributes(cancelButtonAttributes as? [NSAttributedStringKey : Any], for: UIControlState.normal)
    }
    
    func isSearching() -> Bool {
        return searchController.isActive && !searchBarIsEmpty()
    }
    
}
