//
//  CollectionViewCell.swift
//  Movs
//
//  Created by Murilo Gasparetto on 28/02/2018.
//  Copyright © 2018 Murilo Gasparetto. All rights reserved.
//

import UIKit

protocol CollectionViewCellDelegate {
    func favMovie (tag: NSNumber)
}

class CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var movieImg: UIImageView!
    @IBOutlet weak var movieTitle: UILabel!
    @IBOutlet weak var btnFav: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var delegate: CollectionViewCellDelegate!
    
    @IBAction func favAction(_ sender: Any) {
        let btn = sender as! UIButton
        let tag = NSNumber(integerLiteral: btn.tag)
        self.delegate.favMovie(tag: tag)
    }
}
