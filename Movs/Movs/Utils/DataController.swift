//
//  DataController.swift
//  Movs
//
//  Created by Murilo Gasparetto on 02/03/2018.
//  Copyright © 2018 Murilo Gasparetto. All rights reserved.
//

import UIKit
import os.log

extension DataController {
    static func checkFav(id: NSNumber) -> Bool{
        if DataController.savedMovies.count > 0 { //savedMovies sempre vai existir e só faz sentido verificar caso acha algum filme salvo
            if let _ = DataController.savedMovies.index(where: ({$0.id == id})) {
                return true
            } else {
                return false
            }
        } else {
            return false
        }
    }
}

struct DataController {
    //MARK: Archiving Paths
    static let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.appendingPathComponent("movies")
    static var savedMovies = [Movie]()
    static var fetchedMovies = [Movie]()
    
    //MARK: Save and Load
    static func saveMovies(movies: [Movie]) {
        let isSuccessfulSave = NSKeyedArchiver.archiveRootObject(movies, toFile: self.ArchiveURL.path)
        if isSuccessfulSave {
            os_log("Movies succesfully saved", log: OSLog.default, type: .debug)
        } else {
            os_log("Failed to save", log: OSLog.default, type: .error)
        }
    }
    
    static func loadMovies() -> [Movie]? {
        return NSKeyedUnarchiver.unarchiveObject(withFile: self.ArchiveURL.path) as? [Movie]
    }
    
    static func updateSavedMovies(movie: Movie) {
        if movie.fav {
            guard DataController.savedMovies.index(of: movie) != nil else {
                movie.fav = true
                DataController.savedMovies.append(movie)
                return
            }
        } else {
            if let index = DataController.savedMovies.index(where: ({$0.id == movie.id})) {
                movie.fav = false
                DataController.savedMovies.remove(at: index)
            }
            if let index = DataController.fetchedMovies.index(where: ({$0.id == movie.id})) {
                movie.fav = false
                DataController.fetchedMovies[index].fav = false
            }
        }
    }
}
