//
//  ImageManager.swift
//  Movs
//
//  Created by Murilo Gasparetto on 03/03/2018.
//  Copyright © 2018 Murilo Gasparetto. All rights reserved.
//

import UIKit

struct ImageManager {
    static let managerDefault = ImageManager()
    static let imageCache = NSCache<NSString, UIImage>()
    
    private func pathToImg (path: String) -> UIImage? {
        let fullPath = "https://image.tmdb.org/t/p/w300" + path
        let url = URL(string:fullPath)
        if let data = try? Data(contentsOf: url!)
        {
            let image = UIImage(data:data)!
            return image
        } else {
            return nil
        }
    }
    
    func cacheImgFromPath (path: String) -> UIImage? {
        guard let cachedImage = ImageManager.imageCache.object(forKey: path as NSString) else {
            guard let image = ImageManager.managerDefault.pathToImg(path: path) else {
                return nil
            }
            ImageManager.imageCache.setObject(image, forKey: path as NSString)
            return image
        }
        return cachedImage
    }
    
}
