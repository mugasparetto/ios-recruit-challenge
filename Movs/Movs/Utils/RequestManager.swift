//
//  RequestManager.swift
//  Movs
//
//  Created by Murilo Gasparetto on 05/03/2018.
//  Copyright © 2018 Murilo Gasparetto. All rights reserved.
//

import UIKit

struct RequestManager {
    static var page = 1
    
    static func fetchData (displayError: @escaping () -> (), finishedFeeding: @escaping ([Movie]) -> (), completion: @escaping () -> ()) {
        // Set up the URL request
        let endpoint: String = "https://api.themoviedb.org/3/movie/popular?page=\(self.page)&language=en-US&api_key=4e016e66124ebc7e2c5352ef52772613"
        guard let url = URL(string: endpoint) else {
            displayError()
            print("Error: cannot create URL")
            return
        }
        let urlRequest = URLRequest(url: url)
        
        // set up the session
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        // make the request
        let task = session.dataTask(with: urlRequest) {
            (data, response, error) in
            // check for any errors
            guard error == nil else {
                DispatchQueue.main.async {
                    displayError()
                }
                print("error calling GET")
                print(error!)
                return
            }
            // make sure we got data
            guard let responseData = data else {
                DispatchQueue.main.async {
                    displayError()
                }
                print("Error: did not receive data")
                return
            }
            
            // parse the result as JSON, since that's what the API provides
            do {
                guard let r = try JSONSerialization.jsonObject(with: responseData, options: [])
                    as? [String: Any] else {
                        DispatchQueue.main.async {
                            displayError()
                        }
                        print("error trying to convert data to JSON")
                        return
                }
                let rDict = r as NSDictionary
                guard let results = rDict.object(forKey: "results") as? NSArray else {
                    print ("result dictionary found nil")
                    DispatchQueue.main.async {
                        displayError()
                    }
                    return
                }
                var moviesArray = [Movie]()
                for m in results {
                    let movie = Movie.newMovie(m: m as AnyObject)
                    moviesArray.append(movie)
                }
                
                let returnedArray = moviesArray
                finishedFeeding(returnedArray)
                
                DispatchQueue.main.async {
                    completion()
                }
                
            } catch  {
                print("error trying to convert data to JSON")
                DispatchQueue.main.async {
                    displayError()
                }
            }
        }
        task.resume()
        self.page += 1
    }
}
