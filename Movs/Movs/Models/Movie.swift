//
//  Movie.swift
//  Movs
//
//  Created by Murilo Gasparetto on 28/02/2018.
//  Copyright © 2018 Murilo Gasparetto. All rights reserved.
//

import UIKit
import os.log

struct PropertyKey {
    static let id = "id"
    static let title = "title"
    static let release_date = "release_date"
    static let poster_path = "poster_path"
    static let genres = "genres"
    static let overview = "overview"
    static let fav = "fav"
}

class Movie: NSObject, NSCoding {
    //MARK: Properties
    var id: NSNumber
    var title: String
    var release_date: String
    var poster_path: String
    var genres: [String]
    var overview: String
    var fav: Bool
    
    override var description : String {
        return "----------------------\nTITLE: \(self.title)\nFAVORITE: \(self.fav)\n----------------------"
    }
    
    //MARK: Initialization
    required init(id:NSNumber, title:String, year:String, image:String, genres:[String], overview:String, fav:Bool) {
        self.id = id
        self.title = title
        self.release_date = year
        self.poster_path = image
        self.genres = genres
        self.overview = overview
        self.fav = fav
    }
    
    static func newMovie (m: AnyObject) -> Movie{
        let id = m.object(forKey: "id")! as! NSNumber
        let title = m.object(forKey: "title")! as! String
        
        let d = m.object(forKey: "release_date")! as! String
        let date = String(d.split(separator: "-")[0])
        
        let path = m.object(forKey: "poster_path")! as! String
        
        let idsArray = m.object(forKey: "genre_ids")! as! [NSNumber]
        var genres = [String]()
        for id in idsArray {
            genres.append(Genre.genres[id]!)
        }
        
        let overview = m.object(forKey: "overview") as! String
        let fav = DataController.checkFav(id: id)
        
        return self.init(id: id, title: title, year: date, image: path, genres: genres, overview: overview, fav: fav)
    }
    
    func printableGenres() -> String {
        var description = ""
        for s in genres {
            if s == genres.last! {
                description = description + s
            } else {
                description = description + s + ", "
            }
        }
        return description
    }
    
    //MARK: NSCoding
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: PropertyKey.id)
        aCoder.encode(title, forKey: PropertyKey.title)
        aCoder.encode(release_date, forKey: PropertyKey.release_date)
        aCoder.encode(poster_path, forKey: PropertyKey.poster_path)
        aCoder.encode(genres, forKey: PropertyKey.genres)
        aCoder.encode(overview, forKey: PropertyKey.overview)
        aCoder.encode(fav, forKey: PropertyKey.fav)
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        let id = aDecoder.decodeObject(forKey: PropertyKey.id) as! NSNumber
        let title = aDecoder.decodeObject(forKey: PropertyKey.title) as! String
        let release_date = aDecoder.decodeObject(forKey: PropertyKey.release_date) as! String
        let poster_path = aDecoder.decodeObject(forKey: PropertyKey.poster_path) as! String
        let genres = aDecoder.decodeObject(forKey: PropertyKey.genres) as! [String]
        let overview = aDecoder.decodeObject(forKey: PropertyKey.overview) as! String
        let fav = aDecoder.decodeBool(forKey: PropertyKey.fav)
        self.init(id: id, title: title, year: release_date, image: poster_path, genres: genres, overview: overview, fav: fav)
    }
}
